p = [];
document.querySelectorAll("ytd-playlist-video-renderer a.yt-simple-endpoint").forEach(v => {

    if(v.href.includes("/watch?v=")){
        let link = v.href.split('&list=')[0];
        if(!p.includes(link)){
            p.push(link);
        }
    }
});

let json = `{"format":"Piped","version":1,"playlists":[{"name":"WatchLater","type":"playlist","visibility":"private","videos":["${p.join('", "')}"]}]}`
console.log(json)


/* I copied this from https://www.linuxscrew.com/javascript-write-to-file */
var textFileUrl = null;
// Function for generating a text file URL containing given text
function generateTextFileUrl(txt) {
    let fileData = new Blob([txt], {type: 'text/plain'});
    // If a file has been previously generated, revoke the existing URL
    if (textFileUrl !== null) {
        window.URL.revokeObjectURL(textFile);
    }
    textFileUrl = window.URL.createObjectURL(fileData);
    // Returns a reference to the global variable holding the URL
    // Again, this is better than generating and returning the URL itself from the function as it will eat memory if the file contents are large or regularly changing
    return textFileUrl;
};


let jsonurl = generateTextFileUrl(json)
window.location = jsonurl;
