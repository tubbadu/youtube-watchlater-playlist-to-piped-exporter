# Instructions

1. copy the whole [script.js](https://codeberg.org/tubbadu/youtube-watchlater-playlist-to-piped-exporter/raw/commit/6ab7eb45fc78cc2dbfc8bbf5a10ba936cfd8265d/script.js) to clipboard (no need to clone this repo)
2. go to youtube.com, log in, and navigate to the [WatchLater playlist](https://www.youtube.com/playlist?list=WL)
3. open the browser's console (right-click on the webpage in any point and select "Inspect"), then go to the "Console" tab
    - you may see a bunch of warnings and errors. Totally normal, just ignore them
4. paste the whole script in the console and press Enter
    - DISCLAIMER: It is usually not a good idea to to paste into the browser's console random code copied from random strangers on the internet, so do this at your own risk. I am not responsible for any harm caused by this script. I pinky promise that this is not a malicious script :)
5. the page should be automatically redirected to a raw text file. Save it as "watchLater.json" or any other name that you like
6. go to your favourite piped instance, and click on the top right on the "Playlists" tab
7. Click on "Import from JSON/CSV" and select the file you just saved
8. it may require some time, wait for it to finish before refreshing or closing the tab

## notes
- it seems that if one of the videos is private or removed, the import would crash with an error. If this happens, just remove that video from the playlist and repeat the process. If this does not work, I'm really sad :(
